import { useEffect, useRef, useState, useContext } from 'react';
import Router from 'next/router';
import { Row, Col, Card, Button, Alert } from 'react-bootstrap'
import mapboxgl from 'mapbox-gl';
mapboxgl.accessToken = process.env.NEXT_PUBLIC_REACT_APP_MAPBOX_KEY
import MapboxDirections from '@mapbox/mapbox-gl-directions/dist/mapbox-gl-directions'
import UserContext from '../UserContext';

const StreetNavigation=() =>{

	const { user } = useContext(UserContext);

	const[distance, setDistance] = useState(0)
	const[duration, setDuration] = useState(0)
	const[originLong, setOriginLong] = useState(0)
	const[originLat, setOriginLat] = useState(0)
	const[destinationLong, setDestinationLong] = useState(0)
	const[destinationLat, setDestinationLat] = useState(0)
	const[isActive, setIsActive] = useState(false)

	const mapContainerRef = useRef(null);

	useEffect(()=>{
		const map= new mapboxgl.Map({
			container: mapContainerRef.current,
			style:'mapbox://styles/mapbox/streets-v11',
			center:[121.04382, 14.63289],
			zoom:12
		})

		//Instantiate mapbox directions control overlay
		const directions = new MapboxDirections({
			accessToken:mapboxgl.accessToken,
			unit:'metric',
			profile:'mapbox/driving'
		})

		map.addControl(new mapboxgl.NavigationControl(), 'bottom-right');

		//Add Mapbox Directions control overlay to the map
		map.addControl(directions,'top-left')

		//Whenever a route is generated, it returns an array of route objects
		directions.on("route", e =>{
			// console.log(e)
			console.log(e.route)


			//set the states based on the details from mapbox
			if(typeof e.route !== "undefined"){
				//distance
				setDistance(e.route[0].distance)
				//duration
				setDuration(e.route[0].duration)

				// origin (firstelement of the array)
				setOriginLat(e.route[0].legs[0].steps[0].intersections[0].location[1])
				setOriginLong(e.route[0].legs[0].steps[0].intersections[0].location[0])

				// destination(last element of the array)
				let stepCount=(e.route[0].legs[0].steps).length
				setDestinationLat(e.route[0].legs[0].steps[stepCount-1].intersections[0].location[1])
				setDestinationLong(e.route[0].legs[0].steps[stepCount-1].intersections[0].location[0])

			}
		})

		return()=>map.remove()

	},[])

	//set isActive after setting other states(location details)
	useEffect(()=>{
		if(originLat>0 & originLong>0 & destinationLat>0 & destinationLong>0 & distance>0 & duration>0){
			setIsActive(true)
		}
	},[distance,duration, originLong, originLat, destinationLong, destinationLat])



//Record travel details in database
	function recordTravel(){
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/travels`,{
            method: 'POST',
            headers: {
            	'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
			body: JSON.stringify({
		        originLong:originLong,
		        originLat:originLat,
		        destinationLong:destinationLong,
		        destinationLat:destinationLat,
		        duration:duration,
		        distance:distance

			})

		})
		.then(res => {
            return res.json()
        })
		.then(data =>{
			console.log(data);
			if(data=true){
				Router.push('/history')
			}
		})
	
	}


	return(
		<Row>
			<Col xs={12} md={8}>
				<div className="mapContainer" ref={mapContainerRef} />
			</Col>
			<Col xs={12} md={4}>
				{user.id == null?
					<Alert variant="info">
						You must be login to record your travels
					</Alert>
					:

					<Card>
						<Card.Body>
							<Card.Title>
								Record Route
							</Card.Title>
							<Card.Text>
								Origin Longitude:{originLong}
							</Card.Text>
							<Card.Text>
								Origin Latitude:{originLat}
							</Card.Text>
								Destination Longitude:{destinationLong}
							<Card.Text>
								Destination Latitude:{destinationLat}
							</Card.Text>
							<Card.Text>
								Total Distance: {Math.round(distance)} meters
							</Card.Text>
							<Card.Text>
								Total Duration: {Math.round(duration/60)} minutes
							</Card.Text>
							<Card.Text>
							</Card.Text>
							{ isActive === true
								?
									<Button
										variant="success"
										onClick={recordTravel}
									>
									Record
									</Button>
								:
									<Button
										variant="secondary"
										disabled
									>
									Record
									</Button>

							}
						</Card.Body>
					</Card>

				}
			</Col>
		</Row>

		
	)

}

export default StreetNavigation


