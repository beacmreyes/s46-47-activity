import React from 'react';
import Head from 'next/head';
// import StreetNavigation from '../components/StreetNavigation';

//need to load more infoes needed before generating the map
/*Server Error
ReferenceError: XMLHttpRequest is not defined
*/
import dynamic from 'next/dynamic'

const DynamicComponent = dynamic(()=> import('../components/StreetNavigation'), {ssr:false})


export default function Travel(){
	return(
		<React.Fragment>
			<Head>
				<title>Record Your Tavels</title>
			</Head>
			<DynamicComponent />
		</React.Fragment>
	)
}