import React,{ useEffect,useRef, useState } from 'react';
import {Table,Alert,Row,Col,Container} from 'react-bootstrap'
import Head from 'next/head'
import moment from 'moment'
import mapboxgl from 'mapbox-gl';
mapboxgl.accessToken = process.env.NEXT_PUBLIC_REACT_APP_MAPBOX_KEY


const History=() =>{


	// TRAVEL HISTORY TABLE
	const [travelHistory, setTravelHistory] = useState([])
	const [latitude,setLatitude] = useState(0);
	const [longitude,setLongitude] = useState(0);
	const [zoom,setZoom] = useState(0);
	const mapContainerRef = useRef(null) 

	// const tableContainer =

	useEffect(()=>{
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`,{
	        headers: {
	        	'Content-Type': 'application/json',
	            'Authorization': `Bearer ${localStorage.getItem('token')}`
	        }
		})
		.then(res => {
	        return res.json()
	    })
		.then(data =>{
			setTravelHistory(data.travels)
			// if(data.travels==){
			// console.log("h")

			// 	return(
			// 		<React.Fragment>
			// 			<Alert variant="info">
			// 				No Travel History
			// 			</Alert>
			// 		</React.Fragment>

			// 	)				
			// }
		})

	console.log(travelHistory)

	},[])


	const travelRow =travelHistory.map(travel =>{
				console.log(travel)
				
				return (
					<tr>
						<td><a onClick={()=>{setCoordinates(travel.origin.longitude,travel.origin.latitude)}}> {travel.origin.longitude}, {travel.origin.latitude} </a></td>
						<td><a onClick={()=>{setCoordinates(travel.destination.longitude,travel.destination.latitude)}}> {travel.destination.longitude}, {travel.destination.latitude} </a></td>
						<td>{moment(travel.date).format('MMMM D, YYYY')}</td>
						<td>{Math.round(travel.distance)}</td>
						<td>{Math.round(travel.duration)}</td>
					</tr>

				)	
			})




	// MAP


	// States for the mapbox properties
	useEffect(()=>{

		//Instatiate a new Mapbox Map object
		const map = new mapboxgl.Map({
			// Set the container for the map
			container: mapContainerRef.current,
			//Style options for the map
			style: 'mapbox://styles/mapbox/streets-v11',
			center: [longitude,latitude],
			zoom:zoom
		})

		//Add navigation control (the +/- zoom buttons)
		map.addControl(new mapboxgl.NavigationControl(), 'bottom-right')

		//Create a marker
		const marker = new mapboxgl.Marker()
		.setLngLat([longitude,latitude])
		.addTo(map)

		//Clean up and release all resourdes associated with this map when component unmounts
		// Not doing so, will result in an ever-increasing consumption of memory or RAM (since napapatungan lang map) and eventually crashing your device
		return() => map.remove()

	},[latitude,longitude,zoom])

	function setCoordinates(long,lat){
		setLatitude(lat)
		setLongitude(long)
		setZoom(15)

	}



	return(
		<React.Fragment>
				<Row>
					<Col xs={12} md={6}>
								
							<div >
								<Head>
									<title> Travel History</title>
								</Head>
								<Table striped bordered hover>
									<thead>
										<tr>
											<th>Origin</th>
											<th>Destination</th>
											<th>Date</th>
											<th>Distance (m)</th>
											<th>Duration (mins)</th>
										</tr>
									</thead>

									<tbody>
										{travelHistory.length==0?
											<tr>
												<td colspan="5">
													<Alert variant="info">
														No Travel History
													</Alert>
												</td>
											</tr>
										:
											travelRow
									}
									</tbody>
								</Table>
							</div>
					
					</Col>

					<Col xs={12} md={6}>
						<div className = "mapContainer" ref={ mapContainerRef }/>
					</Col>
				</Row>
			
		</React.Fragment>
		
		)


}

export default History
